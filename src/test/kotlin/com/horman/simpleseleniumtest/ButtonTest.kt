package com.horman.simpleseleniumtest

import com.codeborne.selenide.Condition.visible
import com.codeborne.selenide.Configuration
import com.codeborne.selenide.Selenide.element
import com.codeborne.selenide.Selenide.open
import com.codeborne.selenide.logevents.SelenideLogger
import io.qameta.allure.selenide.AllureSelenide
import org.junit.jupiter.api.*

class ButtonTest {

    @BeforeEach
    fun setUpAll() {
        Configuration.browserSize = "1280x800"
        SelenideLogger.addListener("allure", AllureSelenide())
    }

    @BeforeEach
    fun setUp() {
        open("https://www.eurosport.ru/")
    }

    @Test
    fun clickButton() {
        val button = element("a[data-testid='marketing-banner-black-more']")
        button.shouldBe(visible)
        button.click()
        element("a[data-test='subscribe-now']").shouldBe(visible)
    }
}
